# desafio-automacao

Descrição:

**How to:**<br>
Metodos implementados:<br>
    - testCreateNewUserSuccess: testando a criação de um novo usuario com sucesso<br>
    - testRunLoginSuccess: testando a execução de login com sucesso<br>
    - testCreateNewEmployeeSuccess: testando o cadastro de um novo funcionario com sucesso<br>
    - testEditEmplooyeSuccess: testando a opção editar um funcionario com sucesso<br>
    - testDeleteEmplooyeSuccess: testando a opção excluir um funcionario com sucesso <br>
Para todos os medotodos implementados acima, foi realizada a criação de metodos separados para cada ação que o teste realiza.<br>
<br>
<br>
**Tecnologia utilizas:**<br>
    - Selenium WebDriver<br> 
    - JUnit<br>
    - Chromediver (escolher de acordo com seu sistema operacional e versão do Chrome)<br>
    - IntelliJ<br>
    - Java 15.0.1<br>
    - Selenium IDE (para auxiliar na identificação dos elementos na pagina WEB)<br>
<br>
<br>
**Instruções para compilação:**<br>
Caso for executar o codigo em sua maquina, será preciso fazer a modificação do caminho do Chromediver.
Acessar o arquivo Support > Web e no onde está o caminho C:\Users\camil\Drivers\chromedriver.exe, modificar para o caminho que o Chromediver se encontra em sua maquina.


